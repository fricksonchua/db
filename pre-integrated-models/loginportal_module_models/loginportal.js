const mongoose = require('mongoose');

var userRole = mongoose.model('userRole', {
    role: { type: String }
}, 'userRole');

var user = mongoose.model('user', {
    userRole_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'userRole',
        required: true
    },
    username: { type: String, required: true, unique: true },
    officialEmail: {
        type: String,
        required: true,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    password: { type: String, required: true },
    resetKey: String,
    lastLogin: Number
}, 'user');

var student = mongoose.model('student', {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, required: true },
    matricNo: { type: String, required: true }
}, 'student');

var lecturer = mongoose.model('lecturer', {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, required: true }
}, 'lecturer');

var staff = mongoose.model('staff', {
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        required: true
    },
    name: { type: String, required: true }
}, 'staff');

module.exports = {
    userRole: userRole,
    user: user,
    student: student,
    lecturer: lecturer,
    staff: staff
};