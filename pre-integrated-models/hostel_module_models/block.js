const mongoose = require('mongoose');
//const Schema = mongoose.Schema;

var Block = mongoose.model('Block',
    {
        college: {
            type: String,
            required: true,

        },
        name: {
            type: String,
            required: true,
            validate:{
                validator: function(text){
                    return text.length > 0;
                },
                message: "Empty name is not allowed"
            }
        }
    }
);

module.exports = { Block };
