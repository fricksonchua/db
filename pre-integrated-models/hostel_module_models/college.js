const mongoose = require('mongoose');
//const Schema = mongoose.Schema;

var College = mongoose.model('College',
    {
        name:{
            type: String,
            required:true,
            validate:{
                validator: function(text){
                    return text.length > 0;
                },
                message: "Empty name is not allowed"
            }
        },
        address:{type: String}
    }
);

module.exports = { College };
