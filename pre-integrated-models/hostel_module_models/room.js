const mongoose = require('mongoose');
//const Schema = mongoose.Schema;

var Room = mongoose.model('Room',
    {
        roomno: { type: Number, required: true },
        roomtype: { type: String, required: true },
        rentalrate:{ type: Number, required: true },
        block:{ type: String, required: true },
        student: { type: String }
    }
);

module.exports = { Room };
